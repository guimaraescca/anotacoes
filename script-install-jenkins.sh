### Install Jenkins on Fedora ###

# Configurations
curl --silent --location http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo | sudo tee /etc/yum.repos.d/jenkins.repo
rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key

# Installation
dnf install -y jenkins

# Setup Jenkins to startup on boot
systemctl enable jenkins

# Start Jenkins service
systemctl start jenkins
