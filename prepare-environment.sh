# Install packages
echo "Installing packages"
dnf install -y java-1.8.0-openjdk
dnf install -y java-1.8.0-openjdk-devel
dnf install -y community-mysql-server
dnf install -y nginx

# Set up firewall
echo "Setting up firewall"
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --reload

# Start mysql service
echo "Starting mysql service"
systemctl start mysqld

# Mysql configuration
echo "Please configure MySQL"
sudo mysql_secure_installation

# Wait for nginx file to be configured
read -p "Please add the correct configuration to the nginx file in /etc/nginx/nginx.conf . When it is done, press ENTER to continue."

# Start nginx service
echo "Starting nginx service"
systemctl start nginx

# Set up policy for Nginx in SELinux
echo "Setting up policy in SELinux"
setsebool -P httpd_can_network_connect 1

