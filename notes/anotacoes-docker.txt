Docker
------

A implementação mais popular de conteiners. Era open source, hoje é pago. Existe uma Community Edition com limitações livre para uso.

Gerencia conteiners. 
Trabalha com images, "cópias read-only de conteiners".
Docker RUN faz a criação de um novo conteiner a partir de uma imagem.

- docker ps -a
	Lista todos os conteiners criados

- docker attach <>
	Conecta o cliente a um dos conteiners que foram criados

- docker start <>
	Inicializar conteiners
	(-a) 'attach'

- docker stop <>
	Envia SIGTERM para o conteiner em execucao

- docker images
	Lista imagens instaladas no computador

- docker kill <>
	:O

- docker run -d <>
	Roda o conteiner sem prender o terminal ao output

- docker logs <> (--tail 5 --follow)
	Acesso ao arquivo de log que o conteiner está produzindo (STDOUT)

Para remover uma imagem é necessário remover todos os conteiners associados a ela.

- docker kill <>
- docker rm <>
- docker rmi IMAGE

- docker ps -aq | xargs docker rm
	Lista todos os conteiners (ativos e inativos) por ID e os redireciona como argumento para o comando de deleção dos conteiners.

- docker images -q | xargs docker rmi
	Lista os IDs de todas as imagens e os redireciona para deleção.

- docker diff <>
	Diferencas entre o runtime do conteiner e o que a imagem apresenta

- docker commit <>
	Cria um novo conteiner com as modificações feitas no conteiner baseado nela. Adiciona um nova camada as camadas da imagem.

- docker images
	Lista todas as imagens existentes

- docker tag <hash_code> a.com.br/username/myapp

- docker run -v /host_folder:/conteiner_folder -it alpine
	Criar como volume ?	
	Dois caminhos completos, 1 - para o host, 2 - para 

- do

Conteiners dockers

|---------------|
|  Runtime      |
|---------------|
|  Imagem       |
|---------------|


O nome do registro define a imagem.
	registro/usuario/nome:tag

O docker infere que se você não utilizar ? e ? você está referenciando uma imagem do DockerHub.

