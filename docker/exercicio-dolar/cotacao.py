import requests

base = str(input())
response = requests.get(f"https://api.exchangeratesapi.io/latest?base={base.upper()}&symbols=USD,GBP")
json_response = response.json()

print(f"\nDia: {json_response['date']} \
        \nBase: {json_response['base']} \
        \nCotações: \
        \n  Dolar(USD): {json_response['rates']['USD']} \
        \n  Libra(GBP): {json_response['rates']['GBP']}")